﻿using AutoMapper;
using EMS.DataTransfare.Client;
using EMS.DataTransfare.Meeting;
using EMS.DataTransfare.Room;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Concrete
{
  public class ClientService : IClientService
  {
    private IBaseRepo<Client> clientRepo;
    private IBaseRepo<Sector> sectorRepo;
    private IBaseRepo<TimeSlot> timeSlotRepo;
    private readonly EmsContext context;
    private readonly IMapper mapper;
    private readonly IRoomService roomService;

    public ClientService(IBaseRepo<Client> ClientRepo, IBaseRepo<Sector> sectorRepo, EmsContext context, IMapper mapper, IRoomService roomService, IBaseRepo<TimeSlot> timeSlotRepo)
    {
      this.clientRepo = ClientRepo;
      this.context = context;
      this.sectorRepo = sectorRepo;
      this.mapper = mapper;
      this.roomService = roomService;
      this.timeSlotRepo = timeSlotRepo;
    }
    public async Task<int> AddClient(Client client)
    {
      var existClient =  context.Query<Client>().FirstOrDefault(c => c.Mobile == client.Mobile);
      if (existClient != null)
        false.Expect("User with the same mobile already exist!");
      clientRepo.Add(client);
      await context.SaveChangesAsync();
      return client.Id;
    }
    public async Task<int> AddSector(int clientId, Sector sector)
    {
      var client =await clientRepo.GetById(clientId);
      if (client == null)
        false.Expect("Invalid User id");
      var existSector = context.Query<Sector>().FirstOrDefault(s => s.Name == sector.Name);
      if (sector != null)
        false.Expect("Sector already exist");
      client.Sectors.Add(sector);
      await context.SaveChangesAsync();
      return sector.Id;
    }
    public async Task AddTimeSlot(AddClientSectorDto dto)
    {
      var existClient = await clientRepo.GetById(dto.ClientId);
      var existSector = await sectorRepo.GetById(dto.SectorId);

      if (existClient == null || existSector == null)
        false.Expect(ErrorMessages.InvalidData);

      foreach (var time in dto.TimeSlots)
      {
        var timeslot = await timeSlotRepo.GetById(time);
        if (timeslot != null)
        {
          var existRecord = existClient.ClientSectorTimes
                  .FirstOrDefault(x => x.SectorId == dto.SectorId && x.TimeSlotId == time && x.ClientId == dto.ClientId);
          if (existRecord != null)
            false.Expect(ErrorMessages.AlreadyExist);
          existClient.ClientSectorTimes.Add(new ClientSectorTime
          {
            ClientId = dto.ClientId,
            SectorId = dto.SectorId,
            TimeSlotId = time
          });
        }
      }
      var sectorInClient = existClient.Sectors.FirstOrDefault(s => s.Id == dto.SectorId);
      if (sectorInClient == null)
        existClient.Sectors.Add(existSector);
      await context.SaveChangesAsync();

    }

    public async Task<IEnumerable<Client>> MatchedPresenter(MeetingMatchedPresenter dto)
    {
      var timeslot = await timeSlotRepo.GetById(dto.TimeSlotId);
      var presenters = await context.Query<Client>().Include(c => c.ClientSectorTimes).Include(c => c.Meetings).ThenInclude(m=>m.TimeSlot)
        .Where(c => c.ClientType == ClientType.Presenter
        &&!(c.Meetings.Any(m => (m.TimeSlot.Id == dto.TimeSlotId)))
        && c.ClientSectorTimes.Any(cts => cts.SectorId == dto.SectorId
        && cts.TimeSlotId == dto.TimeSlotId)).ToListAsync();

      return presenters;
    }
    public async Task<IEnumerable<AvailableMeetingsDto>> GetAvailableMeetings(int investorId)
    {
      var investor = await clientRepo.GetById(investorId);
      List<AvailableMeetingsDto> result = new List<AvailableMeetingsDto>();
      foreach (var clientsectortime in investor.ClientSectorTimes)
      {
        var matchedPresenter = await MatchedPresenter(new
          MeetingMatchedPresenter
        {
          SectorId = clientsectortime.SectorId,
          TimeSlotId = clientsectortime.TimeSlotId
        });
        var matchedRooms = await roomService.GetMatchedRooms(clientsectortime.TimeSlotId);
        var mappedMatchedPresenters = mapper.Map<IEnumerable<ClientDetailedViewDto>>(matchedPresenter);
        var mappedMatchedRooms = mapper.Map<IEnumerable<RoomViewDto>>(matchedRooms);
        if(matchedRooms.Count()>0 && matchedPresenter.Count() > 0)
          result.Add(new AvailableMeetingsDto { Rooms = mappedMatchedRooms.ToArray(), Presenters = mappedMatchedPresenters.ToArray(), TimeSlotId = clientsectortime.TimeSlotId });
      }
      return result;
    }
    public async Task<AvailableMeetingsDto> GetAvailableMeetingsByTime(int sectorId,int timeSlotId)
    {
      
        var matchedPresenter = await MatchedPresenter(new
          MeetingMatchedPresenter
        {
          SectorId = sectorId,
          TimeSlotId = timeSlotId
        });
        var matchedRooms = await roomService.GetMatchedRooms(timeSlotId);
        var mappedMatchedPresenters = mapper.Map<IEnumerable<ClientDetailedViewDto>>(matchedPresenter);
        var mappedMatchedRooms = mapper.Map<IEnumerable<RoomViewDto>>(matchedRooms);
        if (matchedRooms.Count() > 0 && matchedPresenter.Count() > 0)
          return new AvailableMeetingsDto { Rooms = mappedMatchedRooms.ToArray(), Presenters = mappedMatchedPresenters.ToArray(), TimeSlotId = timeSlotId};
      
      return null;
    }
    public async Task<IEnumerable<Client>> GetClientsByType(ClientType type)
    {
      return await context.Query<Client>().Where(c => c.ClientType == type).ToListAsync();
    }
  }
}
