﻿using AutoMapper;
using EMS.DataTransfare.Room;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Concrete
{
  public class RoomService : IRoomService
  {
    private readonly IBaseRepo<Room> roomRepo;
    private readonly IBaseRepo<Hotel> hotelRepo;
    private readonly IBaseRepo<TimeSlot> timeslotRepo;
    private readonly EmsContext context;
    private readonly IMapper mapper;

    public RoomService(IBaseRepo<Hotel> hotelRepo,IBaseRepo<Room> roomRepo,EmsContext context,IMapper mapper, IBaseRepo<TimeSlot> timeslotRepo)
    {
      this.roomRepo = roomRepo;
      this.hotelRepo = hotelRepo;
      this.context = context;
      this.mapper = mapper;
      this.timeslotRepo = timeslotRepo;
    }
    public async Task<int> AddRoom(AddRoomDto roomDto)
    {
      var room = mapper.Map<AddRoomDto,Room>(roomDto);
      room.TimeSlots = new HashSet<TimeSlot>();
      var hotel = await hotelRepo.GetById(roomDto.HotelId);
      if(hotel == null)
        false.Expect(ErrorMessages.InvalidHotelId);
      if (hotel.Rooms.Any(r => r.Name == roomDto.Name))
        false.Expect(ErrorMessages.AlreadyExist);
      room.Hotel = hotel;
      var insertedRoom = roomRepo.Add(room);
      await context.SaveChangesAsync();
      foreach (var time in roomDto.TimeSlots)
      {
        //var existTimeSlot = context.Query<TimeSlot>().FirstOrDefault(t => t.From == time.From && t.To == time.To);
        var timeslot = await timeslotRepo.GetById(time);
        //if (existTimeSlot != null)
        //{
        //  if (!insertedRoom.TimeSlots.Contains(existTimeSlot))
        //    insertedRoom.TimeSlots.Add(existTimeSlot);
        //}
        //else
        //{
        //  var entryTimeSlot = timeslotRepo.Add(mapper.Map<TimeSlot>(time));
        //  await context.SaveChangesAsync();
        //  insertedRoom.TimeSlots.Add(entryTimeSlot);
        //}
        if(timeslot != null)
          insertedRoom.TimeSlots.Add(timeslot);
      }
      await context.SaveChangesAsync();
      return insertedRoom.Id;


    }

    public async Task<IEnumerable<Room>> GetHotelRooms(int hotelId)
    {
      return await context.Query<Room>().Include(r => r.Hotel).Where(r => r.Hotel.Id == hotelId).ToListAsync();
    }

    public async Task<IEnumerable<Room>> GetMatchedRooms(int TimeSlotId)
    {
      return await context.Query<Room>().Include(r => r.Meetings).ThenInclude(m => m.TimeSlot).Include(r => r.TimeSlots)
        .Where(r => r.TimeSlots.Any(t => t.Id == TimeSlotId) 
              && !(r.Meetings.Any(m => (m.TimeSlot.Id == TimeSlotId))))
        .ToListAsync();
    }
  }
}
