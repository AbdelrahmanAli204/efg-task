﻿using AutoMapper;
using EMS.DataTransfare.Meeting;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Concrete
{
  public class MeetingService : IMeetingService
  {
    private readonly IBaseRepo<Meeting> meetingRepo;

    private readonly IMapper mapper;
    private readonly IBaseRepo<Client> clientRepo;
    private readonly EmsContext context;
    private readonly IBaseRepo<Room> roomRepo;
    private readonly IBaseRepo<Sector> sectorRepo;
    private readonly IClientService clientService;
    private readonly IRoomService roomService;

    public MeetingService(IBaseRepo<Meeting> meetingRepo,IMapper mapper
      ,IBaseRepo<Client> clientRepo,EmsContext context
      ,IBaseRepo<Room> roomRepo,IBaseRepo<Sector> sectorRepo,
      IClientService clientService,IRoomService roomService)
    {
      this.meetingRepo = meetingRepo;
      this.mapper = mapper;
      this.clientRepo = clientRepo;
      this.context = context;
      this.roomRepo = roomRepo;
      this.sectorRepo = sectorRepo;
      this.clientService = clientService;
      this.roomService = roomService;
    }
    public async Task Add(AddMeetingDto dto)
    {
      var presenter = await clientRepo.GetById(dto.PresenterId);
      var investor = await clientRepo.GetById(dto.InvestorId);
      var test = context.Query<Client>().Include(c => c.Meetings).ThenInclude(m => m.TimeSlot)
        .FirstOrDefault(c => c.Id == dto.PresenterId);
      var room = await roomRepo.GetById(dto.RoomId);
      var sector = await sectorRepo.GetById(dto.SectorId);
      if (presenter == null || investor == null || room == null && sector == null)
        false.Expect(ErrorMessages.InvalidData);
      //check if sector with timeslot already for clients
     
      var investorSectorTimeSlots = context.Query<ClientSectorTime>().Include(c=>c.TimeSlot).FirstOrDefault(x => x.ClientId == dto.InvestorId
                                                &&x.SectorId == dto.SectorId   && x.TimeSlotId == dto.TimeSlotId);
      var presenterSectorTimeSlots = context.Query<ClientSectorTime>().Include(c => c.TimeSlot).FirstOrDefault(x => x.ClientId == dto.PresenterId
                                                   && x.SectorId == dto.SectorId && x.TimeSlotId == dto.TimeSlotId);

      if(investorSectorTimeSlots == null || presenterSectorTimeSlots == null)
        false.Expect(ErrorMessages.HasNoSectorTimes);

      var meetings = context.Query<Meeting>().Include(m => m.Clients).Include(m => m.TimeSlot);
      var investorExistMeeting = meetings.FirstOrDefault(m => m.Clients.Any(c => c.Id == investor.Id)
                       && m.TimeSlot.Id == dto.TimeSlotId);
      var presenterExistMeeting = meetings.FirstOrDefault(m => m.Clients.Any(c => c.Id == presenter.Id)
                      && m.TimeSlot.Id == dto.TimeSlotId);
      //checking if meeting already
      if (investorExistMeeting != null || presenterExistMeeting != null)
        false.Expect(ErrorMessages.HasAlreadyMeeting);

      var mappedMeeting = mapper.Map<Meeting>(dto);
      mappedMeeting.Room = room;
      mappedMeeting.Clients.Add(investor);
      mappedMeeting.Sector = sector;
      mappedMeeting.Clients.Add(presenter);
      mappedMeeting.TimeSlot = investorSectorTimeSlots.TimeSlot;
      meetingRepo.Add(mappedMeeting);
      await context.SaveChangesAsync();
    }

    public async Task<IEnumerable<Meeting>> GetMeetings()
    {
     return await meetingRepo.GetAll();
    }
  }
}
