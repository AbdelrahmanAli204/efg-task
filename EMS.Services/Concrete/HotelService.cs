﻿using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Concrete
{
  public class HotelService : IHotelService
  {
    private IBaseRepo<Hotel> hotelRepo;
    private EmsContext context;

    public HotelService(IBaseRepo<Hotel> hotelRepo,EmsContext context)
    {
      this.hotelRepo = hotelRepo;
      this.context = context;
    }
    public async Task<int> AddHotel(Hotel hotel)
    {
      var result = hotelRepo.Add(hotel);
      await context.SaveChangesAsync();
      return hotel.Id;
    }
  }
}
