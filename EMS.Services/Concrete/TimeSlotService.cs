﻿using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using System.Threading.Tasks;

namespace EMS.Services.Concrete
{
  public class TimeSlotService : ITimeSlotService
  {
    private readonly IBaseRepo<TimeSlot> timeslotRepo;
    private readonly EmsContext context;
    public TimeSlotService(IBaseRepo<TimeSlot> timeslotRepo, EmsContext context)
    {
      this.timeslotRepo = timeslotRepo;
      this.context = context;
    }
    public async Task<int> AddTimeSlot(TimeSlot timeslot)
    {
      var result = timeslotRepo.Add(timeslot);
      await context.SaveChangesAsync();
      return result.Id;

    }
  }
}
