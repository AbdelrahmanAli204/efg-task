﻿using EMS.DataTransfare.Client;
using EMS.DataTransfare.Meeting;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Abstract
{
  public interface IClientService
  {
    Task<int> AddClient(Client client);
    Task<int> AddSector(int clientId, Sector sector);
    Task AddTimeSlot(AddClientSectorDto dto);
    Task<IEnumerable<Client>> MatchedPresenter(MeetingMatchedPresenter dto);
    Task<IEnumerable<AvailableMeetingsDto>> GetAvailableMeetings(int investorId);
    Task<IEnumerable<Client>> GetClientsByType(ClientType type);
    Task<AvailableMeetingsDto> GetAvailableMeetingsByTime(int sectorId, int timeSlotId);



  }
}
