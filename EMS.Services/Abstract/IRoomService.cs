﻿using EMS.DataTransfare.Room;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Abstract
{
  public interface IRoomService
  {
    Task<int> AddRoom(AddRoomDto room);
    Task<IEnumerable<Room>> GetHotelRooms(int hotelId);
    Task<IEnumerable<Room>> GetMatchedRooms(int TimeSlotId);
  }
}
