﻿using EMS.DataTransfare.Meeting;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Abstract
{
  public interface IMeetingService
  {
    Task Add(AddMeetingDto dto);
    Task<IEnumerable<Meeting>> GetMeetings();
  }
}
