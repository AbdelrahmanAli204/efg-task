﻿using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Services.Abstract
{
  public interface ITimeSlotService
  {
    Task<int> AddTimeSlot(TimeSlot timeslot);
  }
}
