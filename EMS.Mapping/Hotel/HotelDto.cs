﻿using EMS.DataTransfare.Room;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare
{
  public class HotelViewDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public RoomViewDto[] Rooms { get; set; }
  }
}
