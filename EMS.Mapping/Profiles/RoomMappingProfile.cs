﻿using AutoMapper;
using EMS.DataTransfare.Room;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class RoomMappingProfile : Profile
  {
    public RoomMappingProfile()
    {
      CreateMap<EMS.Domain.Entities.Room, RoomViewDto>();

      CreateMap<AddRoomDto, EMS.Domain.Entities.Room>()
        .ForMember(room => room.TimeSlots, conf => conf.Ignore());
    }
  }
}
