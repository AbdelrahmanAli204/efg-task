﻿using AutoMapper;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class HotelMappingProfile : Profile
  {
    public HotelMappingProfile()
    {
      CreateMap<KeyValuePairDto, Hotel>().ReverseMap();
      CreateMap<Hotel, HotelViewDto>();
    }
  }
}
