﻿using AutoMapper;
using EMS.DataTransfare.Client;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class ClientMappingProfile : Profile
  {
    public ClientMappingProfile()
    {
       CreateMap<AddClientDto, EMS.Domain.Entities.Client>();
      CreateMap<EMS.Domain.Entities.Client, ClientDetailedViewDto>();
    }
  }
}
