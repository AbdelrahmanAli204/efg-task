﻿using AutoMapper;
using EMS.DataTransfare.Meeting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class MeetingMappingProfile : Profile
  {
    public MeetingMappingProfile()
    {
      CreateMap<AddMeetingDto, EMS.Domain.Entities.Meeting>();
      CreateMap< EMS.Domain.Entities.Meeting, ViewMeetingDto>();
    }
  }
}
