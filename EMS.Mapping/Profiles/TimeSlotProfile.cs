﻿using AutoMapper;
using EMS.DataTransfare.TimeSlot;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class TimeSlotProfile : Profile
  {
    public TimeSlotProfile()
    {
      CreateMap< TimeSlotDto, EMS.Domain.Entities.TimeSlot>().ReverseMap();
    }
  }
}
