﻿using AutoMapper;
using EMS.DataTransfare.Client.Sector;
using EMS.DataTransfare.TimeSlot;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Profiles
{
  public class SectorMappingProfile : Profile
  {
    public SectorMappingProfile()
    {
      CreateMap<ClientSectorTime, SectorTimeDetailedViewDto>()
        .ForMember(dto => dto.SectorName, conf => conf.MapFrom(c => c.Sector.Name))
        .ForMember(dto => dto.Id , conf => conf.MapFrom(c=>c.SectorId));
      CreateMap<Sector, KeyValuePairDto>().ReverseMap();
    }
  }
}
