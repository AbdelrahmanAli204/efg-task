﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.TimeSlot
{
  public class TimeSlotDto
  {
    public int Id { get; set; }
    public DateTime From { get; set; }
    public DateTime To { get; set; }
  }
}
