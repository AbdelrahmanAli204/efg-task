﻿using EMS.DataTransfare.Client;
using EMS.DataTransfare.Room;
using EMS.DataTransfare.TimeSlot;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Meeting
{
  public class AddMeetingDto
  {
    public int RoomId { get; set; }
    public int SectorId { get; set; }
    public int PresenterId { get; set; }
    public int InvestorId { get; set; }
    public int TimeSlotId { get; set; }
  }
  public class MeetingMatchedPresenter
  {
    public int SectorId { get; set; }
    public int TimeSlotId { get; set; }
  }
  public class ViewMeetingDto
  {
    public int Id { get; set; }
    public TimeSlotDto TimeSlot { get; set; }
    public RoomViewDto Room { get; set; }
    public KeyValuePairDto Sector { get; set; }
    public ClientDetailedViewDto[] Clients { get; set; }
  }
}
