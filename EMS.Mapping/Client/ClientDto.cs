﻿using EMS.DataTransfare.Client.Sector;
using EMS.DataTransfare.Room;
using EMS.DataTransfare.TimeSlot;
using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace EMS.DataTransfare.Client
{
  public class AddClientDto
  {
    public string Name { get; set; }
    public string Mobile { get; set; }
    public ClientType ClientType { get; set; }
  }
  public class ClientDetailedViewDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Mobile { get; set; }
    public ClientType ClientType { get; set; }
    public SectorTimeDetailedViewDto[] ClientSectorTimes { get; set; }
  }
  public class AddClientSectorDto
  { 
    public int ClientId { get; set; }
    public int SectorId { get; set; }
    public int[] TimeSlots { get; set; }
  }
  public class AvailableMeetingsDto
  {
    public ClientDetailedViewDto[] Presenters { get; set; }
    public RoomViewDto[] Rooms { get; set; }
    public int TimeSlotId { get; set; }

  }
}
