﻿using EMS.DataTransfare.TimeSlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace EMS.DataTransfare.Client.Sector
{
  public class SectorTimeDetailedViewDto
  {
    public int Id { get; set; }
    public string SectorName { get; set; }
    public TimeSlotDto TimeSlot { get; set; }
  }
  public class AddSectorDto
  { 
    public string Name { get; set; }

  }
}
