﻿using EMS.DataTransfare.TimeSlot;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare.Room
{
  public class RoomViewDto 
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public TimeSlotDto[] TimeSlots { get; set; }
  }
  public class AddRoomDto
  {
    public string Name { get; set; }
    public int HotelId { get; set; }
    public int[] TimeSlots { get; set; }
  }
  public class MeetingMatchedRoom
  {
    public DateTime From { get; set; }
  }
}
