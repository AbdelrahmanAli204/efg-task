﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.DataTransfare
{
  public class KeyValuePairDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
