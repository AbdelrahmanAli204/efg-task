﻿// <auto-generated />
using System;
using EMS.Domain.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EMS.Domain.Migrations
{
    [DbContext(typeof(EmsContext))]
    partial class EmsContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ClientMeeting", b =>
                {
                    b.Property<int>("ClientsId")
                        .HasColumnType("int");

                    b.Property<int>("MeetingsId")
                        .HasColumnType("int");

                    b.HasKey("ClientsId", "MeetingsId");

                    b.HasIndex("MeetingsId");

                    b.ToTable("client_meetings");
                });

            modelBuilder.Entity("ClientSector", b =>
                {
                    b.Property<int>("ClientsId")
                        .HasColumnType("int");

                    b.Property<int>("SectorsId")
                        .HasColumnType("int");

                    b.HasKey("ClientsId", "SectorsId");

                    b.HasIndex("SectorsId");

                    b.ToTable("client_sectors");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientType")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<string>("Mobile")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("dbo.clients");
                });

            modelBuilder.Entity("EMS.Domain.Entities.ClientSectorTime", b =>
                {
                    b.Property<int>("ClientId")
                        .HasColumnType("int");

                    b.Property<int>("TimeSlotId")
                        .HasColumnType("int");

                    b.Property<int>("SectorId")
                        .HasColumnType("int");

                    b.HasKey("ClientId", "TimeSlotId", "SectorId");

                    b.HasIndex("SectorId");

                    b.HasIndex("TimeSlotId");

                    b.ToTable("dbo.client_sector_times");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Hotel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("dbo.hotels");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Meeting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<int?>("RoomId")
                        .HasColumnType("int");

                    b.Property<int>("SectorId")
                        .HasColumnType("int");

                    b.Property<int?>("TimeSlotId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("RoomId");

                    b.HasIndex("SectorId");

                    b.HasIndex("TimeSlotId");

                    b.ToTable("dbo.meetings");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Room", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<int?>("HotelId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("HotelId");

                    b.ToTable("dbo.rooms");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Sector", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("dbo.sectors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 57, 996, DateTimeKind.Local).AddTicks(8999),
                            EntityState = 0,
                            Name = "Finance",
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 57, 999, DateTimeKind.Local).AddTicks(8968)
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(330),
                            EntityState = 0,
                            Name = "Restaurants",
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(382)
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(753),
                            EntityState = 0,
                            Name = "Real Estate",
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(762)
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(781),
                            EntityState = 0,
                            Name = "IT",
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(786)
                        });
                });

            modelBuilder.Entity("EMS.Domain.Entities.TimeSlot", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DeletedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("EntityState")
                        .HasColumnType("int");

                    b.Property<DateTime>("From")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("To")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("dbo.time_slots");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 8, DateTimeKind.Local).AddTicks(8245),
                            EntityState = 0,
                            From = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(2664),
                            To = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(4366),
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 8, DateTimeKind.Local).AddTicks(8300)
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(5930),
                            EntityState = 0,
                            From = new DateTime(2021, 3, 14, 1, 43, 58, 9, DateTimeKind.Local).AddTicks(5970),
                            To = new DateTime(2021, 3, 14, 2, 43, 58, 9, DateTimeKind.Local).AddTicks(6210),
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(5948)
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6252),
                            EntityState = 0,
                            From = new DateTime(2021, 3, 14, 2, 43, 58, 9, DateTimeKind.Local).AddTicks(6262),
                            To = new DateTime(2021, 3, 14, 3, 43, 58, 9, DateTimeKind.Local).AddTicks(6267),
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6255)
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6271),
                            EntityState = 0,
                            From = new DateTime(2021, 3, 14, 3, 43, 58, 9, DateTimeKind.Local).AddTicks(6280),
                            To = new DateTime(2021, 3, 14, 4, 43, 58, 9, DateTimeKind.Local).AddTicks(6284),
                            UpdatedAt = new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6275)
                        });
                });

            modelBuilder.Entity("RoomTimeSlot", b =>
                {
                    b.Property<int>("RoomsId")
                        .HasColumnType("int");

                    b.Property<int>("TimeSlotsId")
                        .HasColumnType("int");

                    b.HasKey("RoomsId", "TimeSlotsId");

                    b.HasIndex("TimeSlotsId");

                    b.ToTable("rooms_timeslots");
                });

            modelBuilder.Entity("ClientMeeting", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Client", null)
                        .WithMany()
                        .HasForeignKey("ClientsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.Meeting", null)
                        .WithMany()
                        .HasForeignKey("MeetingsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ClientSector", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Client", null)
                        .WithMany()
                        .HasForeignKey("ClientsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.Sector", null)
                        .WithMany()
                        .HasForeignKey("SectorsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("EMS.Domain.Entities.ClientSectorTime", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Client", "Client")
                        .WithMany("ClientSectorTimes")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.Sector", "Sector")
                        .WithMany("ClientSectorTimes")
                        .HasForeignKey("SectorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.TimeSlot", "TimeSlot")
                        .WithMany("ClientSectorTimes")
                        .HasForeignKey("TimeSlotId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Client");

                    b.Navigation("Sector");

                    b.Navigation("TimeSlot");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Meeting", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Room", "Room")
                        .WithMany("Meetings")
                        .HasForeignKey("RoomId");

                    b.HasOne("EMS.Domain.Entities.Sector", "Sector")
                        .WithMany("Meetings")
                        .HasForeignKey("SectorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.TimeSlot", "TimeSlot")
                        .WithMany()
                        .HasForeignKey("TimeSlotId");

                    b.Navigation("Room");

                    b.Navigation("Sector");

                    b.Navigation("TimeSlot");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Room", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Hotel", "Hotel")
                        .WithMany("Rooms")
                        .HasForeignKey("HotelId");

                    b.Navigation("Hotel");
                });

            modelBuilder.Entity("RoomTimeSlot", b =>
                {
                    b.HasOne("EMS.Domain.Entities.Room", null)
                        .WithMany()
                        .HasForeignKey("RoomsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EMS.Domain.Entities.TimeSlot", null)
                        .WithMany()
                        .HasForeignKey("TimeSlotsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("EMS.Domain.Entities.Client", b =>
                {
                    b.Navigation("ClientSectorTimes");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Hotel", b =>
                {
                    b.Navigation("Rooms");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Room", b =>
                {
                    b.Navigation("Meetings");
                });

            modelBuilder.Entity("EMS.Domain.Entities.Sector", b =>
                {
                    b.Navigation("ClientSectorTimes");

                    b.Navigation("Meetings");
                });

            modelBuilder.Entity("EMS.Domain.Entities.TimeSlot", b =>
                {
                    b.Navigation("ClientSectorTimes");
                });
#pragma warning restore 612, 618
        }
    }
}
