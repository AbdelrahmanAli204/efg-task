﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EMS.Domain.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dbo.clients",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mobile = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClientType = table.Column<int>(type: "int", nullable: false),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dbo.hotels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.hotels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dbo.sectors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.sectors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dbo.time_slots",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    From = table.Column<DateTime>(type: "datetime2", nullable: false),
                    To = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.time_slots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dbo.rooms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HotelId = table.Column<int>(type: "int", nullable: true),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.rooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.rooms_dbo.hotels_HotelId",
                        column: x => x.HotelId,
                        principalTable: "dbo.hotels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "client_sectors",
                columns: table => new
                {
                    ClientsId = table.Column<int>(type: "int", nullable: false),
                    SectorsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_client_sectors", x => new { x.ClientsId, x.SectorsId });
                    table.ForeignKey(
                        name: "FK_client_sectors_dbo.clients_ClientsId",
                        column: x => x.ClientsId,
                        principalTable: "dbo.clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_client_sectors_dbo.sectors_SectorsId",
                        column: x => x.SectorsId,
                        principalTable: "dbo.sectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dbo.client_sector_times",
                columns: table => new
                {
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    TimeSlotId = table.Column<int>(type: "int", nullable: false),
                    SectorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.client_sector_times", x => new { x.ClientId, x.TimeSlotId, x.SectorId });
                    table.ForeignKey(
                        name: "FK_dbo.client_sector_times_dbo.clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "dbo.clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.client_sector_times_dbo.sectors_SectorId",
                        column: x => x.SectorId,
                        principalTable: "dbo.sectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.client_sector_times_dbo.time_slots_TimeSlotId",
                        column: x => x.TimeSlotId,
                        principalTable: "dbo.time_slots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dbo.meetings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeSlotId = table.Column<int>(type: "int", nullable: true),
                    RoomId = table.Column<int>(type: "int", nullable: true),
                    SectorId = table.Column<int>(type: "int", nullable: false),
                    EntityState = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.meetings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.meetings_dbo.rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "dbo.rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.meetings_dbo.sectors_SectorId",
                        column: x => x.SectorId,
                        principalTable: "dbo.sectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.meetings_dbo.time_slots_TimeSlotId",
                        column: x => x.TimeSlotId,
                        principalTable: "dbo.time_slots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "rooms_timeslots",
                columns: table => new
                {
                    RoomsId = table.Column<int>(type: "int", nullable: false),
                    TimeSlotsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rooms_timeslots", x => new { x.RoomsId, x.TimeSlotsId });
                    table.ForeignKey(
                        name: "FK_rooms_timeslots_dbo.rooms_RoomsId",
                        column: x => x.RoomsId,
                        principalTable: "dbo.rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_rooms_timeslots_dbo.time_slots_TimeSlotsId",
                        column: x => x.TimeSlotsId,
                        principalTable: "dbo.time_slots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "client_meetings",
                columns: table => new
                {
                    ClientsId = table.Column<int>(type: "int", nullable: false),
                    MeetingsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_client_meetings", x => new { x.ClientsId, x.MeetingsId });
                    table.ForeignKey(
                        name: "FK_client_meetings_dbo.clients_ClientsId",
                        column: x => x.ClientsId,
                        principalTable: "dbo.clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_client_meetings_dbo.meetings_MeetingsId",
                        column: x => x.MeetingsId,
                        principalTable: "dbo.meetings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "dbo.sectors",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "EntityState", "Name", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 3, 14, 0, 43, 57, 996, DateTimeKind.Local).AddTicks(8999), null, 0, "Finance", new DateTime(2021, 3, 14, 0, 43, 57, 999, DateTimeKind.Local).AddTicks(8968) },
                    { 2, new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(330), null, 0, "Restaurants", new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(382) },
                    { 3, new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(753), null, 0, "Real Estate", new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(762) },
                    { 4, new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(781), null, 0, "IT", new DateTime(2021, 3, 14, 0, 43, 58, 2, DateTimeKind.Local).AddTicks(786) }
                });

            migrationBuilder.InsertData(
                table: "dbo.time_slots",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "EntityState", "From", "To", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 3, 14, 0, 43, 58, 8, DateTimeKind.Local).AddTicks(8245), null, 0, new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(2664), new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(4366), new DateTime(2021, 3, 14, 0, 43, 58, 8, DateTimeKind.Local).AddTicks(8300) },
                    { 2, new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(5930), null, 0, new DateTime(2021, 3, 14, 1, 43, 58, 9, DateTimeKind.Local).AddTicks(5970), new DateTime(2021, 3, 14, 2, 43, 58, 9, DateTimeKind.Local).AddTicks(6210), new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(5948) },
                    { 3, new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6252), null, 0, new DateTime(2021, 3, 14, 2, 43, 58, 9, DateTimeKind.Local).AddTicks(6262), new DateTime(2021, 3, 14, 3, 43, 58, 9, DateTimeKind.Local).AddTicks(6267), new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6255) },
                    { 4, new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6271), null, 0, new DateTime(2021, 3, 14, 3, 43, 58, 9, DateTimeKind.Local).AddTicks(6280), new DateTime(2021, 3, 14, 4, 43, 58, 9, DateTimeKind.Local).AddTicks(6284), new DateTime(2021, 3, 14, 0, 43, 58, 9, DateTimeKind.Local).AddTicks(6275) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_client_meetings_MeetingsId",
                table: "client_meetings",
                column: "MeetingsId");

            migrationBuilder.CreateIndex(
                name: "IX_client_sectors_SectorsId",
                table: "client_sectors",
                column: "SectorsId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.client_sector_times_SectorId",
                table: "dbo.client_sector_times",
                column: "SectorId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.client_sector_times_TimeSlotId",
                table: "dbo.client_sector_times",
                column: "TimeSlotId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.meetings_RoomId",
                table: "dbo.meetings",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.meetings_SectorId",
                table: "dbo.meetings",
                column: "SectorId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.meetings_TimeSlotId",
                table: "dbo.meetings",
                column: "TimeSlotId");

            migrationBuilder.CreateIndex(
                name: "IX_dbo.rooms_HotelId",
                table: "dbo.rooms",
                column: "HotelId");

            migrationBuilder.CreateIndex(
                name: "IX_rooms_timeslots_TimeSlotsId",
                table: "rooms_timeslots",
                column: "TimeSlotsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "client_meetings");

            migrationBuilder.DropTable(
                name: "client_sectors");

            migrationBuilder.DropTable(
                name: "dbo.client_sector_times");

            migrationBuilder.DropTable(
                name: "rooms_timeslots");

            migrationBuilder.DropTable(
                name: "dbo.meetings");

            migrationBuilder.DropTable(
                name: "dbo.clients");

            migrationBuilder.DropTable(
                name: "dbo.rooms");

            migrationBuilder.DropTable(
                name: "dbo.sectors");

            migrationBuilder.DropTable(
                name: "dbo.time_slots");

            migrationBuilder.DropTable(
                name: "dbo.hotels");
        }
    }
}
