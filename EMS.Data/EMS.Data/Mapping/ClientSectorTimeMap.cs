﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class ClientSectorTimeMap : IEntityTypeConfiguration<ClientSectorTime>
  {
    public void Configure(EntityTypeBuilder<ClientSectorTime> builder)
    {
      builder.ToTable("dbo.client_sector_times");
      builder.HasKey(cst => new { cst.ClientId, cst.TimeSlotId, cst.SectorId });
      builder.HasOne(c => c.Sector).WithMany(c=>c.ClientSectorTimes);
      builder.HasOne(c => c.Client).WithMany(c => c.ClientSectorTimes);
      builder.HasOne(c => c.TimeSlot).WithMany(c => c.ClientSectorTimes);

    }
  }
}
