﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class RoomMap : IEntityTypeConfiguration<Room>
  {
    public void Configure(EntityTypeBuilder<Room> builder)
    {
      builder.ToTable("dbo.rooms");
      builder.HasMany(r => r.Meetings).WithOne(m => m.Room);
      builder.HasMany(r => r.TimeSlots).WithMany(t => t.Rooms).UsingEntity(join => join.ToTable("rooms_timeslots"));
    }
  }
}
