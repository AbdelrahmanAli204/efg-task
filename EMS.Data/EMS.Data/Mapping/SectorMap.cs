﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class SectorMap : IEntityTypeConfiguration<Sector>
  {
    public void Configure(EntityTypeBuilder<Sector> builder)
    {
      builder.ToTable("dbo.sectors");
      builder.HasData(
        new Sector { Id = 1,EntityState = 0, Name = "Finance" },
        new Sector { Id = 2,EntityState = 0, Name = "Restaurants" },
        new Sector { Id = 3,EntityState = 0, Name = "Real Estate" },
        new Sector { Id = 4,EntityState = 0, Name = "IT" }
        ) ;
    }
  }
}
