﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class MeetingMap : IEntityTypeConfiguration<Meeting>
  {
    public void Configure(EntityTypeBuilder<Meeting> builder)
    {
      builder.ToTable("dbo.meetings");
      builder.HasOne(m => m.Sector).WithMany(c => c.Meetings).IsRequired();
    }
  }
}
