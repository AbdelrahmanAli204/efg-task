﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class ClientMap : IEntityTypeConfiguration<Client>
  {
    public void Configure(EntityTypeBuilder<Client> builder)
    {
      builder.ToTable("dbo.clients");
      builder.HasMany(c => c.Sectors).WithMany(s => s.Clients).UsingEntity(join => join.ToTable("client_sectors"));
      builder.HasMany(c => c.Meetings).WithMany(m => m.Clients).UsingEntity(join => join.ToTable("client_meetings"));

    }
  }
}
