﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class TimeSlotMap : IEntityTypeConfiguration<TimeSlot>
  {
    public void Configure(EntityTypeBuilder<TimeSlot> builder)
    {
      builder.ToTable("dbo.time_slots");
      builder.HasData(
        new TimeSlot { Id = 1,From = DateTime.Now, To = DateTime.Now, EntityState = 0 },
        new TimeSlot { Id = 2,From = DateTime.Now.AddHours(1), To = DateTime.Now.AddHours(2), EntityState = 0 },
        new TimeSlot { Id = 3,From = DateTime.Now.AddHours(2), To = DateTime.Now.AddHours(3), EntityState = 0 },
        new TimeSlot { Id = 4,From = DateTime.Now.AddHours(3), To = DateTime.Now.AddHours(4), EntityState = 0 }
      );
    }
  }
}
