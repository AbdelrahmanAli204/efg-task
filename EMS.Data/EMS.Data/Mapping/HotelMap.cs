﻿using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Mapping
{
  public class HotelMap : IEntityTypeConfiguration<Hotel>
  {
    public void Configure(EntityTypeBuilder<Hotel> builder)
    {
      builder.ToTable("dbo.hotels");
      builder.HasMany(h => h.Rooms).WithOne(r => r.Hotel);
    }
  }
}
