﻿using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Domain.Concrete
{
  public class BaseRepo<T> : IBaseRepo<T> where T: BaseEntity, new()
  {
    private DbSet<T> entities;
    private EmsContext _context;

    public BaseRepo(EmsContext context)
    {
      entities = context.Set<T>();
      this._context = context;
    }
    public async Task<T>  GetById(int id)
    {
      return await entities.FindAsync(id);
    }
    public T Add(T entity)
    {
     return entities.Add(entity).Entity;
    }
    public async Task<IEnumerable<T>> GetAll()
    {
      return await entities.ToListAsync();
    }
    public T Update(int id, T entity)
    {
      var entityToUpdate = entities.Find(id);
      if (entityToUpdate == null)
        return default(T);
      var res = entities.Attach(entity).Entity;
      _context.Entry(res).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
      return res;
    }
    public async Task Delete(int id)
    {
      var entity = await entities.FindAsync(id);
      _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
    }

  }
}
