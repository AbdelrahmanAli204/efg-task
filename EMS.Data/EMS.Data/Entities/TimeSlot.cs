﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Entities
{
  public class TimeSlot : BaseEntity
  {
    public TimeSlot()
    {
      ClientSectorTimes = new HashSet<ClientSectorTime>();
      Rooms = new HashSet<Room>();
      //RoomTimeSlots = new HashSet<RoomTimeSlot>();
    }
    public DateTime From { get; set; }
    public DateTime To { get; set; }
    public virtual ISet<Room> Rooms { get; set; }
    //public virtual ISet<RoomTimeSlot> RoomTimeSlots { get; set; }

    public virtual ISet<ClientSectorTime> ClientSectorTimes { get; set; }
  }
}
