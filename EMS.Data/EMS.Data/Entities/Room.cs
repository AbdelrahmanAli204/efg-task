﻿using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Entities
{
  public class Room : BaseEntity
  {
    public Room()
    {
      //RoomTimeSlots = new HashSet<RoomTimeSlot>();
      Meetings = new HashSet<Meeting>();
      TimeSlots = new HashSet<TimeSlot>();
    }
    public string Name { get; set; }


    public virtual Hotel Hotel { get; set; }
    public virtual ISet<TimeSlot> TimeSlots { get; set; }
    //public virtual ISet<RoomTimeSlot> RoomTimeSlots { get; set; }

    public virtual ISet<Meeting> Meetings { get; set; }
  }
}
