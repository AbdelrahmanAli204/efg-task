﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Entities
{
  public class Hotel : BaseEntity
  {
    public Hotel()
    {
      Rooms = new HashSet<Room>();
    }
    public string Name { get; set; }

    public virtual ISet<Room> Rooms { get; set; }
  }
}
