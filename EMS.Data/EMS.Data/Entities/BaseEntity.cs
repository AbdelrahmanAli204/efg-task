﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Domain.Entities
{
  public class BaseEntity
  {
    [Key]
    public int Id { get; set; }
    public EntityState EntityState { get; set; }

    public DateTime? CreatedAt { get; private set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; } = DateTime.Now;
    public DateTime? DeletedAt { get; set; }
  }
  public enum EntityState
  {
    Active,
    Occupied,
    Deleted
  }

}
