﻿using System.Collections.Generic;

namespace EMS.Domain.Entities
{
  public class Sector : BaseEntity
  {
    public Sector()
    {
      Clients = new HashSet<Client>();
      Meetings = new HashSet<Meeting>();
      ClientSectorTimes = new HashSet<ClientSectorTime>();
    }
    public string Name { get; set; }
    public virtual ISet<Client> Clients { get; set; }
    public virtual ISet<Meeting> Meetings { get; set; }
    public virtual ISet<ClientSectorTime> ClientSectorTimes { get; set; }
  }
}

