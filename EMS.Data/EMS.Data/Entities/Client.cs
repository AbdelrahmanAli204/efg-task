﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace EMS.Domain.Entities
{
  public class Client : BaseEntity 
  {
    public Client()
    {
      Sectors = new HashSet<Sector>();
      Meetings = new HashSet<Meeting>();
      ClientSectorTimes = new HashSet<ClientSectorTime>();
    }

    public string Name { get; set; }
    public string Mobile { get; set; }
    public ClientType ClientType { get; set; }

    public virtual ISet<Meeting> Meetings { get; set; }
    public virtual ISet<Sector> Sectors { get; set; }
    public virtual ISet<ClientSectorTime> ClientSectorTimes { get; set; }
    }
  public enum ClientType
  {
    [EnumMember(Value = "Investor")]
    Investor,
    [EnumMember(Value = "Presenter")]
    Presenter
  }
}

