﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Entities
{
  public class RoomTimeSlot
  {
    public int RoomId { get; set; }
    public virtual Room Room { get; set; }
    public int TimeSlotId { get; set; }
    public virtual TimeSlot TimeSlot { get; set; }
  }
}
