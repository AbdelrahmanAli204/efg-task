﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Domain.Entities
{
  public class ClientSectorTime
  {
    public int ClientId { get; set; }
    public virtual Client Client { get; set; }
    public int TimeSlotId { get; set; }
    public virtual TimeSlot TimeSlot { get; set; }
    public int SectorId { get; set; }
    public virtual Sector Sector { get; set; }
  }
}
