﻿using System;
using System.Collections.Generic;

namespace EMS.Domain.Entities
{
  public class Meeting : BaseEntity
  {
    public Meeting()
    {
      Clients = new HashSet<Client>();
    }
    public virtual TimeSlot TimeSlot { get; set; }
    public virtual Room Room { get; set; }
    public virtual Sector Sector { get; set; }
    public virtual ISet<Client> Clients { get; set; }
  }
}
  