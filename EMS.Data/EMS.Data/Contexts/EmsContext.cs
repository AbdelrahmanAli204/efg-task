﻿using EMS.Domain.Abstract;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EMS.Domain.Contexts
{
  public class EmsContext  : DbContext, IQueryFactory
  {
    public EmsContext(DbContextOptions<EmsContext> options) : base(options)
    { 
      
    }
    

    public IQueryable<T> Query<T>() where T : class
    {
      return Set<T>();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      modelBuilder.ApplyConfigurationsFromAssembly(typeof(EmsContext).Assembly);
    }
  }
}
