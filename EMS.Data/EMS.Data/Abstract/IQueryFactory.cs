﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMS.Domain.Abstract
{
  public interface IQueryFactory
  {
    public IQueryable<T> Query<T>() where T : class;
  }
}
