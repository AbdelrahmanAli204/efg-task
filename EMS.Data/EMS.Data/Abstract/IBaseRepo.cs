﻿using EMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Domain.Abstract
{
  public interface IBaseRepo<T> where T : BaseEntity 
  {
    T Add(T entity);
    Task<T> GetById(int id);
    Task<IEnumerable<T>> GetAll();
    T Update(int id, T entity);
    Task Delete(int id);


  }
}
