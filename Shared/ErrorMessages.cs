﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
  public static class ErrorMessages
  {
    public static string InvalidData = "Invalid Data";
    public static string InvalidHotelId = "Invalid Hotel Id";
    public static string AlreadyExist = "Already Exist";
    public static string InvalidClientId = "Invalid Client Id";
    public static string HasNoSectorTimes = "Either Presenter or Investor has no time slots for that sector!";
    public static string HasAlreadyMeeting = "Either Presenter or Investor has already meeting with that time!";
    public static string NotFound = "Not Found";
  }
}
