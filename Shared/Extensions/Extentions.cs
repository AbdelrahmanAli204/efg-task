﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Extensions
{
  public static class Extentions
  {
    public static void Expect(this bool prediate,string message)
    {
      if (!prediate)
        throw new Exception(message);
      return;
    }
  }
}
