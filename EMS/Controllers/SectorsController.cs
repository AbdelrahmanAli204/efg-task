﻿using AutoMapper;
using EMS.DataTransfare;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class SectorsController : Controller
  {
    private readonly IBaseRepo<Sector> sectorRepo;
    private readonly EmsContext context;
    private readonly IMapper mapper;

    public SectorsController(IBaseRepo<Sector> sectorRepo,EmsContext context , IMapper mapper)
    {
      this.sectorRepo = sectorRepo;
      this.context = context;
      this.mapper = mapper;
    }
    [HttpPost]
    public async Task<IActionResult> AddSector([FromBody] KeyValuePairDto dto)
    {
      if (string.IsNullOrWhiteSpace(dto.Name))
        return BadRequest(ErrorMessages.InvalidData);
      var sector = sectorRepo.Add(mapper.Map<Sector>(dto));
      await context.SaveChangesAsync();
      return Ok(sector.Id);
    }
    public async Task<IActionResult> GetSectors()
    {
      var sectors = await sectorRepo.GetAll();
      var mappedSectors = mapper.Map<IEnumerable<KeyValuePairDto>>(sectors);
      return Ok(mappedSectors);
    }
  }
}
