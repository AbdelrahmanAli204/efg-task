﻿using AutoMapper;
using EMS.DataTransfare.Meeting;
using EMS.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class MeetingsController : Controller
  {
    private readonly IMeetingService meetingService;
    private readonly IMapper mapper;

    public MeetingsController(IMeetingService meetingService,IMapper mapper)
    {
      this.meetingService = meetingService;
      this.mapper = mapper;
    }
    [HttpPost]
    public async Task<IActionResult> AddMeeting([FromBody]AddMeetingDto dto)
    {
      await meetingService.Add(dto);
      return Ok();
    }
    public async Task<IActionResult> GetMeetings()
    {
      var meetings =await meetingService.GetMeetings();
      var mappedMeetings = mapper.Map<IEnumerable<ViewMeetingDto>>(meetings);
      return Ok(mappedMeetings);
    }
  }
}
