﻿using AutoMapper;
using EMS.DataTransfare;
using EMS.DataTransfare.Client;
using EMS.DataTransfare.Meeting;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class ClientsController : Controller
  {
    private readonly IClientService clientService;
    private readonly IMapper mapper;
    private readonly EmsContext context;
    private readonly IBaseRepo<Client> clientRepo;

    public ClientsController(IClientService clientService,IMapper mapper,EmsContext context,IBaseRepo<Client> clientRepo)
    {
      this.clientService = clientService;
      this.mapper = mapper;
      this.context = context;
      this.clientRepo = clientRepo;
    }
    public async Task<IActionResult> AddClient([FromBody]AddClientDto dto)
    {
      if (string.IsNullOrWhiteSpace(dto.Mobile))
        return BadRequest(ErrorMessages.InvalidData);
      var client = mapper.Map<Client>(dto);
      var response = await clientService.AddClient(client);
      return Ok(response);
    }
    public async Task<IActionResult> AddClientSector([FromBody] AddClientSectorDto dto)
    {
      await clientService.AddTimeSlot(dto);
     
      return Ok();
    }
    public async Task<IActionResult> GetClient([FromRoute]int id)
    {
      var client =await clientRepo.GetById(id);
      var mappedClient = mapper.Map<ClientDetailedViewDto>(client);
      return Ok(mappedClient);
    }
    public async Task<ActionResult> GetClientsByType(ClientType type)
    {
      var clients = await clientService.GetClientsByType(type);
      var mappedClients = mapper.Map<IEnumerable<ClientDetailedViewDto>>(clients);
      return Ok(mappedClients);
    }
    public async Task<IActionResult> GetAvailableMeetings(int id)
    {
      var investor = await clientRepo.GetById(id);
      if (investor == null)
        false.Expect(ErrorMessages.NotFound);
      var result = await clientService.GetAvailableMeetings(id);
      return Ok(result);
    }
    [Route("api/clients/GetAvialableMeeting/{timeslotId:int}/{sectorId:int}")]
    public async Task<IActionResult> GetAvialableMeeting(int sectorId,int timeslotId)
    {
      var meeting = await clientService.GetAvailableMeetingsByTime(sectorId, timeslotId);
      var mappedMeeting = mapper.Map<AvailableMeetingsDto>(meeting);
      return Ok(mappedMeeting);
    }
    [Route("api/clients/GetMatchedPresenter/{timeslotId:int}/{sectorId:int}")]
    public async Task<IActionResult> GetMatchedPresenter(int timeslotId,int sectorId)
    {
      var presenters = await clientService.MatchedPresenter(new MeetingMatchedPresenter { TimeSlotId = timeslotId,SectorId = sectorId });
      var mappedPresenters = mapper.Map<IEnumerable<ClientDetailedViewDto>>(presenters);
      return Ok(mappedPresenters);
    }

  }
}
