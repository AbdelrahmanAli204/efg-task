﻿using AutoMapper;
using EMS.DataTransfare;
using EMS.DataTransfare.Room;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class RoomsController : Controller
  {
    private readonly EmsContext context;
    private readonly IBaseRepo<Room> roomRepo;

    public RoomsController(IMapper mapper, IRoomService roomService,EmsContext context
      ,IBaseRepo<Room> roomRepo)
    {
      this.mapper = mapper;
      this.roomService = roomService;
      this.context = context;
      this.roomRepo = roomRepo;
    }

    public IMapper mapper { get; }
    public IRoomService roomService { get; }
    [HttpPost]
    public async Task<IActionResult> AddRoom([FromBody]AddRoomDto roomDto)
    {
      if (string.IsNullOrWhiteSpace(roomDto.Name))
        false.Expect(ErrorMessages.InvalidData);

      var res = await roomService.AddRoom(roomDto);
      return Ok(res);
    }
    public async Task<IActionResult> GetHotelRooms([FromRoute]int id)
    {
      var rooms =await roomService.GetHotelRooms(id);
      var mappedRooms = mapper.Map<IEnumerable<RoomViewDto>>(rooms);
      return Ok(mappedRooms);
    }
    public async Task<IActionResult> GetRoom(int id)
    {
      var room = await roomRepo.GetById(id);
      var mappedRoom = mapper.Map<RoomViewDto>(room);
      return Ok(mappedRoom);
    }
  }
}
