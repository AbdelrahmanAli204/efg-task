﻿using AutoMapper;
using EMS.DataTransfare.TimeSlot;
using EMS.Domain.Abstract;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class TimeslotsController : Controller
  {
    private readonly IBaseRepo<TimeSlot> timeslotRepo;
    private readonly EmsContext context;
    private readonly IMapper mapper;

    public TimeslotsController(IBaseRepo<TimeSlot> timeslotRepo,EmsContext context,IMapper mapper)
    {
      this.timeslotRepo = timeslotRepo;
      this.context = context;
      this.mapper = mapper;
    }
    public async Task<IActionResult> GetAll()
    {
      var timeslots = await timeslotRepo.GetAll();
      var mappedTimeslot = mapper.Map<IEnumerable<TimeSlotDto>>(timeslots);
      return Ok(mappedTimeslot);
    }
    [HttpPost]
    public async Task<IActionResult> Add(TimeSlotDto dto)
    {
      var addedTimeslot =  timeslotRepo.Add(mapper.Map<TimeSlot>(dto));
      await context.SaveChangesAsync();
      return Ok(mapper.Map<TimeSlotDto>(addedTimeslot));
    }

  }
}
