﻿using AutoMapper;
using EMS.DataTransfare;
using EMS.Domain.Abstract;
using EMS.Domain.Entities;
using EMS.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Controllers
{
  public class HotelsController : Controller
  {
    private readonly IBaseRepo<Hotel> hotelRepo;

    public HotelsController(IMapper mapper,IHotelService hotelService,IBaseRepo<Hotel> hotelRepo)
    {
      this.mapper = mapper;
      this.hotelService = hotelService;
      this.hotelRepo = hotelRepo;
    }

    public IMapper mapper { get; }
    public IHotelService hotelService { get; }

    [HttpPost]
    public async Task<IActionResult> AddHotel([FromBody]KeyValuePairDto hotelDto)
    {
      if (string.IsNullOrWhiteSpace(hotelDto.Name))
        false.Expect("Invalid Data");
      var hotel = mapper.Map<Hotel>(hotelDto);

      var res = await hotelService.AddHotel(hotel);
      return Ok(res);

    }
    public async Task<IActionResult> GetHotel([FromRoute]int id)
    {
      var hotel = await hotelRepo.GetById(id);
      var mappedHotel = mapper.Map<HotelViewDto>(hotel);
      return Ok(mappedHotel);
    }
    public async Task<IActionResult> GetAll()
    {
      var hotels = await hotelRepo.GetAll();
      var mappedHotels = mapper.Map<IEnumerable<Hotel>,IEnumerable<HotelViewDto>>(hotels);
      return Ok(mappedHotels);
    }
  }
}
