
using EMS.Domain.Abstract;
using EMS.Domain.Concrete;
using EMS.Domain.Contexts;
using EMS.Domain.Entities;
using EMS.Logic.Configurations;
using EMS.Services.Abstract;
using EMS.Services.Concrete;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Text.Json.Serialization;

namespace EMS
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddCors();
      services.ConfigureAutoMapper();
      //services.AddScoped<DbContext, EmsContext>();
      services.AddScoped(typeof(IBaseRepo<>), typeof(BaseRepo<>));
      services.AddScoped<IHotelService, HotelService>();
      services.AddScoped<IMeetingService, MeetingService>();
      services.AddScoped<IRoomService, RoomService>();
      services.AddScoped<ITimeSlotService, TimeSlotService>();
      services.AddScoped<IClientService, ClientService>();
      services.AddDbContext<EmsContext>(options => options
      .UseLazyLoadingProxies()
      .UseSqlServer(Configuration["ConnectionStrings:EMSDatabase"]));


      services.AddControllers().AddJsonOptions(options=> {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
  
      });

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.ConfigureExceptionHandler();
      }
      app.UseRouting();
      app.UseCors(builder=> {
        builder.AllowAnyOrigin();
        builder.AllowAnyMethod();
        builder.AllowAnyHeader();
      });

      app.UseAuthorization();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute("Default", "api/{controller}/{action}/{id?}");
      });
      //app.UseSpa(spa =>
      //{

      //  spa.Options.SourcePath = "../Client";
      //  if (env.IsDevelopment())
      //  {
      //    spa.UseAngularCliServer(npmScript: "start");
      //  }
      //});


    }
  }
}
