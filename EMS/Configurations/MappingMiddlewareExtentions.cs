﻿using EMS.DataTransfare;
using EMS.DataTransfare.Profiles;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMS.Logic.Configurations
{
  public static class MappingMiddlewareExtentions
  {
    public static void ConfigureAutoMapper(this IServiceCollection services)
    {
      services.AddAutoMapper(typeof(Startup).Assembly, typeof(ClientMappingProfile).Assembly);
    }
  }
}
