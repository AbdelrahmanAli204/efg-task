import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientViewData } from 'src/app/models';

@Component({
  selector: 'app-reservation-investor-list',
  templateUrl: './reservation-investor-list.component.html',
  styleUrls: ['./reservation-investor-list.component.css']
})
export class ReservationInvestorListComponent implements OnInit {
 investors : ClientViewData[];
 constructor(private route : ActivatedRoute,private api : ApiService) { }

  ngOnInit(): void {
    this.api.GetClients("Investor").subscribe(investors=>{
      this.investors = investors;
    });
  
  }

}
