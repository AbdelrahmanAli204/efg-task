import { ToastrService } from 'ngx-toastr';
import { ClientViewData } from 'src/app/models';
import { ApiService } from './../../api.service';
import { AvailableMeetingsView, AddMeetingDto } from './../../models';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.css']
})
export class ReserveComponent implements OnInit {
 
  investorId : number;
  sectorId : number;
  timeslotId : number;
  meeting : AvailableMeetingsView;
  data : AddMeetingDto = {presenterId :0 ,timeSlotId : 0 , investorId : 0 , roomId : 0,sectorId : 0};
  constructor(private route : ActivatedRoute,private api : ApiService,private notify : ToastrService,private router : Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
        this.investorId = parseInt(params.investorId);
        this.sectorId = parseInt(params.sectorId);

        this.timeslotId = parseInt(params.timeslotId);
        this.data.timeSlotId = this.timeslotId;
        this.data.sectorId = this.sectorId;
        this.data.investorId = this.investorId;
        this.api.GetAvailableMeeting(this.sectorId,this.timeslotId).subscribe(meeting=>{
          this.meeting = meeting;
        });
    });
  }
  save(){
    if(this.data.investorId == 0 || this.data.presenterId == 0 || this.data.roomId == 0 || this.data.sectorId == 0 || this.data.timeSlotId == 0)
      return;
    this.data.roomId = parseInt(this.data.roomId+'');
    this.data.presenterId = parseInt(this.data.presenterId+'');
    this.api.AddMeeting(this.data).subscribe(res=>{
      this.notify.success("Data saved successfully");
      this.router.navigate(['/Home']);
    },error=>{
      this.notify.error(error);
    });
  }
}
