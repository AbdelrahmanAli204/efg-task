import { ApiService } from './../../api.service';
import { AvailableMeetingsView } from './../../models';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';

@Component({
  selector: 'app-reservation-sectors',
  templateUrl: './reservation-sectors.component.html',
  styleUrls: ['./reservation-sectors.component.css']
})
export class ReservationSectorsComponent implements OnInit {
  @Input("sectors") sectors : any[];
  @Input("investorId") investorId : number;
  availableMeetings : AvailableMeetingsView[];
  modalRef : BsModalRef;
  result : any[] = [];

  @Output() onAssignClick = new  EventEmitter<boolean>();
  constructor(private modalService : BsModalService,private api : ApiService) { }

  ngOnInit(): void {
    this.sectors = _.groupBy(this.sectors, s=>s.sectorName);
    this.sectors = _.map(this.sectors, (data,sector)=> {return {sector,data}});
    this.api.GetAvailableMeetings(this.investorId).subscribe(meetings=>{
      this.availableMeetings = meetings;
      this.sectors.forEach(sector => {
        sector.data.forEach(sectorData => {
          let foundmeetings = [];
          foundmeetings = _.filter(this.availableMeetings, meeting=> meeting.timeSlotId == sectorData.timeSlot.id);
          if(foundmeetings.length >0)
            this.result.push({timeslotId : sectorData.timeSlot.id,foundmeetings});
        });
      });

    });
    
  }
  
  existMeeting(timeSlotId : number){
      if(this.availableMeetings != null || this.availableMeetings != undefined){
      let foundMeeting = this.availableMeetings.find(m => m.timeSlotId == timeSlotId);
      return (foundMeeting!=null || foundMeeting != undefined);
      }
      return false;
  }
  closeModal(){
    this.onAssignClick.emit(true);
  }


}
