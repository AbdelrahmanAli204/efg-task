import { ApiService } from './../../api.service';
import { MeetingViewDto } from './../../models';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {
  meetings : MeetingViewDto[];
  constructor(private api : ApiService) { }

  ngOnInit(): void {
    this.api.GetMeetings().subscribe(meetings=>{
      this.meetings = meetings;
    });
    
  }

}
