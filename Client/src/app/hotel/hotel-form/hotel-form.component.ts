import { HotelForm } from './../../models';
import { ApiService } from './../../api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-hotel-form',
  templateUrl: './hotel-form.component.html',
  styleUrls: ['./hotel-form.component.css']
})
export class HotelFormComponent implements OnInit {
  data: HotelForm = {name : ''};
  constructor(private api : ApiService,private notify : ToastrService,private router : Router) { }
  @ViewChild("hotelForm") hotelForm;
  ngOnInit(): void {
  }
  save(){
    this.api.AddHotel(this.data).subscribe((data)=>{
      this.notify.success("Hotel has been added succesfully");
      this.router.navigate([`rooms/add/` + data]);
    },(error)=>{
      this.notify.error(error);
    });
  }

}
