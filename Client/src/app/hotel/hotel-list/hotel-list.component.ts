import { HotelViewDto } from './../../models';
import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {
  hotels : HotelViewDto[] = [];
  constructor(private api : ApiService) { }

  ngOnInit(): void {
    this.api.GetHotels().subscribe(hotels=>{
      this.hotels = hotels;
    });
  }

}
