import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { HotelListComponent } from './hotel/hotel-list/hotel-list.component';
import { HotelFormComponent } from './hotel/hotel-form/hotel-form.component';
import { FormsModule } from '@angular/forms';
import { RoomFormComponent } from './room/room-form/room-form.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientFormComponent } from './client/client-form/client-form.component';
import { ClientEditComponent } from './client/client-edit/client-edit.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { RoomViewComponent } from './room/room-view/room-view.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { ReservationInvestorListComponent } from './reservation/reservation-investor-list/reservation-investor-list.component';
import { ClientViewComponent } from './client/client-view/client-view.component';
import { ReservationSectorsComponent } from './reservation/reservation-sectors/reservation-sectors.component';
import { ReserveComponent } from './reservation/reserve/reserve.component';
import { ReservationListComponent } from './reservation/reservation-list/reservation-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    HotelFormComponent,
    HotelListComponent,
    RoomFormComponent,
    ClientFormComponent,
    ClientEditComponent,
    RoomListComponent,
    RoomViewComponent,
    ClientListComponent,
    ReservationInvestorListComponent,
    ClientViewComponent,
    ReservationSectorsComponent,
    ReserveComponent,
    ReservationListComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
