import { Timestamp } from "rxjs";

export interface HotelForm
{
    name : string;
}
export interface Room{
    id : number;
    name: string;
    hotelId : number;
    timeSlots : number[];
}
export interface TimeSlot{
    id : number;
    from : string;
    to : string;
    day: Date;
}
export interface ClientForm{
    name:string;
    mobile:string;
    clientType : any;
}
export interface PresenterForm extends ClientForm{
    
}
export interface InvestorForm extends ClientForm{
    
}
export interface ClientSectorTimes{
    clientId : number;
    sectorId : number;
    timeSlots: TimeSlot[];
}
export interface KeyValuePair{
    id : number;
    name : string;
}
export interface ClientViewData{
    id : number;
    name : string;
    mobile : string;
    clientType : number;
    clientSectorTimes : SectorTimeDetailedView[];

}
export interface SectorTimeDetailedView{
    id : number;
    sectorName : string;
    timeSlots : TimeSlot[];
}
export interface HotelViewDto{
    id : number;
    name : string;
    rooms : Room[];
}
export interface AvailableMeetingsView{
    presenters : ClientViewData[];
    rooms : Room[];
    timeSlotId : number;
}
export interface AddMeetingDto{
    roomId :number;
    sectorId : number;
    presenterId : number;
    investorId : number;
    timeSlotId : number;
}
export interface MeetingViewDto{
    id : number;
    timeSlot : TimeSlot;
    room : Room;
    sector : KeyValuePair;
    clients : ClientViewData[];
}