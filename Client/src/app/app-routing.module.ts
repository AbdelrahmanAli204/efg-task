import { ReservationListComponent } from './reservation/reservation-list/reservation-list.component';
import { ReserveComponent } from './reservation/reserve/reserve.component';
import { ReservationInvestorListComponent } from './reservation/reservation-investor-list/reservation-investor-list.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { RoomViewComponent } from './room/room-view/room-view.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { RoomFormComponent } from './room/room-form/room-form.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelListComponent } from './hotel/hotel-list/hotel-list.component';
import { HotelFormComponent } from './hotel/hotel-form/hotel-form.component';
import { ClientFormComponent } from './client/client-form/client-form.component';
import { ClientEditComponent } from './client/client-edit/client-edit.component';

const routes: Routes = 
[
  {path : "Home", component : HomeComponent},
  {path : "", component : HomeComponent, pathMatch : 'full'},
  {path : "hotels", component : HotelListComponent},
  {path : "hotels/add",component : HotelFormComponent},
  {path : "rooms/add/:hotelId",component : RoomFormComponent},
  {path : "clients/:type",component : ClientListComponent},
  {path : "clients/add/:type",component:ClientFormComponent},
  {path : "clients/edit/:clientId",component:ClientEditComponent},
  {path : "hotels/:hotelId/rooms", component : RoomListComponent},
  {path : "hotels/:hotelId/rooms/:roomId", component : RoomViewComponent},
  {path : "reservations/investors",component: ReservationInvestorListComponent},
  {path : "reservations/investors/:investorId/sector/:sectorId/time/:timeslotId" , component:ReserveComponent},
  {path : "reservations" , component: ReservationListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
