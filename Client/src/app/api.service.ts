import { environment } from './../environments/environment';
import { HotelForm, Room, ClientSectorTimes, KeyValuePair, ClientViewData, ClientForm, HotelViewDto, TimeSlot, AvailableMeetingsView, AddMeetingDto, MeetingViewDto } from './models';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }
  private _url(apiRoute){
    return environment.baseUrl + apiRoute;
  }
  public AddHotel(formData : HotelForm) : Observable<number>{
    return this.http.post<number>(this._url("/api/hotels/addhotel"),formData);
  }
  public AddRoom(RoomData : Room) : Observable<number>{
    return this.http.post<number>(this._url("/api/rooms/addroom"),RoomData);
  }
  public GetRoom(id : number) : Observable<Room>{
    return this.http.get<Room>(this._url("/api/rooms/getroom/"+id));
  }
  public AddClient(clientData : ClientForm) : Observable<number>{
    return this.http.post<number>(this._url("/api/clients/addclient"),clientData);
  }
  public AddClientSectorTimes(sectorTimesData: ClientSectorTimes) {
    return this.http.post(this._url("/api/clients/addclientsector"),sectorTimesData); 
  }
  public GetSectors(){
    return this.http.get<KeyValuePair[]>(this._url("/api/sectors/getsectors"));
  }
  public GetClient(id:number) : Observable<ClientViewData>{
    return this.http.get<ClientViewData>(this._url("/api/clients/getclient/"+id));
  }
  public GetClients(type:string) : Observable<ClientViewData[]>{
    return this.http.get<ClientViewData[]>(this._url("/api/clients/GetClientsByType/"+type));
  }
  public GetHotels() : Observable<HotelViewDto[]>{
    return this.http.get<HotelViewDto[]>(this._url("/api/hotels/getall"));
  }
  public GetHotel(id : number) : Observable<HotelViewDto>{
    return this.http.get<HotelViewDto>(this._url("/api/hotels/GetHotel/"+id));
  }
  public GetHotelRooms(id : number) : Observable<Room[]>{
    return this.http.get<Room[]>(this._url("/api/rooms/gethotelrooms/"+id));
  }
  public GetTimeSlots(): Observable<TimeSlot[]>{
    return this.http.get<TimeSlot[]>(this._url("/api/timeslots/getall"));
  }
  public GetAvailableMeetings(investorId) : Observable<AvailableMeetingsView[]>{
    return this.http.get<AvailableMeetingsView[]>(this._url("/api/clients/GetAvailableMeetings/"+investorId));
  }
  public GetAvailableMeeting(sectorId:number,timeslotId: number) :Observable<AvailableMeetingsView>{
    return this.http.get<AvailableMeetingsView>(this._url("/api/clients/GetAvialableMeeting/"+timeslotId+"/"+sectorId));
  }
  public GetMatchedPresenters(sectorId : number,timeslotId : number){
    return this.http.get<ClientViewData[]>(this._url("/api/clients/GetMatchedPresenter/"+timeslotId+"/"+sectorId));
  }
  public AddMeeting(meeting : AddMeetingDto) {
    return this.http.post(this._url("/api/meetings/addmeeting"),meeting);
  }
  public GetMeetings() : Observable<MeetingViewDto[]>{
    return this.http.get<MeetingViewDto[]>(this._url("/api/meetings/GetMeetings"));
  }
}
