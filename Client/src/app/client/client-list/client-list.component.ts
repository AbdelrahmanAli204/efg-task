import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientViewData } from 'src/app/models';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  type : string;
  clients : ClientViewData[];
  constructor(private route : ActivatedRoute,private api : ApiService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
        this.type= params.type;
        this.api.GetClients(this.type).subscribe(clients=>{
          this.clients = clients;
        });
    });
  }

}
