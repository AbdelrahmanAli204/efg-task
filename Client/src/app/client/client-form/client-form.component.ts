import { ClientForm } from '../../models';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../../api.service';
import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
  data : ClientForm = { name : '',mobile :'', clientType : -1}
  constructor(private api : ApiService,private notify : ToastrService,private router : Router,private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
        this.data.clientType = params.type;
    });
  }
  save(){
    if(this.data.name.length == 0 || this.data.mobile.length == 0)
      return;
    this.api.AddClient(this.data).subscribe((res)=>{
      this.notify.success("Client added succesfully");
      this.router.navigate([`clients/edit/${res}`,{type:this.data.clientType}]);

    },(error)=>{
      this.notify.error(error);
    })
  }

}
