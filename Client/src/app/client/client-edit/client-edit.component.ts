import { KeyValuePair, ClientViewData, TimeSlot } from '../../models';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientSectorTimes } from 'src/app/models';
import * as _ from 'lodash';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {
  data : ClientSectorTimes = {clientId :0,sectorId : 0 , timeSlots :[]};
  sectorsData : KeyValuePair[];
  clientData : ClientViewData;
  clientType : string= '';
  allTimes : TimeSlot[];
  selectedSecotr : KeyValuePair;
  constructor(private route : ActivatedRoute,private api : ApiService,private notify : ToastrService,private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      this.data.clientId = parseInt(params.clientId);
      this.clientType = params.type;
      
    this.api.GetClient(this.data.clientId).subscribe((clientData)=>{
      this.clientData = clientData;
      this.api.GetTimeSlots().subscribe(times=>{
        this.allTimes = times;
      });
    });
    });
    this.api.GetSectors().subscribe(sectorsData =>{
      this.sectorsData = sectorsData;
    },(error)=>{
      this.notify.error(error);
    })
  }

  save(){
    if(this.data.sectorId == 0 || this.data.clientId == 0 || this.data.timeSlots.length == 0)
      return;
    let sectorIdString : string= this.data.sectorId+'';
    this.data.sectorId = parseInt(sectorIdString);
    this.api.AddClientSectorTimes(this.data).subscribe((res)=>{
      this.notify.success("Inserted Successfully");
    },(error)=>{
      this.notify.error(error);
    })
  }
  insertTimeSlot(timeslotId,value){
    if(value)
      this.data.timeSlots.push(timeslotId);
    else{
       _.remove(this.data.timeSlots, t=>t == timeslotId);
    }
  }


}
