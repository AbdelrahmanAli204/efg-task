import { Component, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ClientViewData } from 'src/app/models';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.css']
})
export class ClientViewComponent implements OnInit {
  @Input("reservationFlag") reservationFlag;
  @Input("client") client : ClientViewData;

  modalRef : BsModalRef;
  constructor(private modal : BsModalService) { }

  ngOnInit(): void {
  }

  openModal(template: TemplateRef<any>) {
    
    this.modalRef = this.modal.show(template,{class : 'modal-xl'});
  }
  closeModal(state){
    if(state){
      this.modalRef.hide();
    }
  }
}
