import { ToastrService } from 'ngx-toastr';
import { ApiService } from './../../api.service';
import { Room, TimeSlot } from './../../models';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-room-form',
  templateUrl: './room-form.component.html',
  styleUrls: ['./room-form.component.css']
})
export class RoomFormComponent implements OnInit {

  constructor(private route : ActivatedRoute,private api : ApiService,private notify : ToastrService) { }
  data : Room= {name :'', timeSlots : [],hotelId : 0,id :0};
  // timeSlot  : TimeSlot= {id: 0 ,from :'',to:'',day : new Date()};
  // timeSlotFlag : boolean = false;
  timeslots : TimeSlot[];
  @ViewChild("timeslotControl") timeslotControl : ElementRef;

  hotelId : number;
  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      this.hotelId=parseInt(params.hotelId);
      this.data.hotelId = this.hotelId;
    });
    this.api.GetTimeSlots().subscribe(times=>{
      this.timeslots = times;
    });
  }
  insertTimeSlot(timeslotId,value){
    if(value)
      this.data.timeSlots.push(timeslotId);
    else{
       _.remove(this.data.timeSlots, t=>t == timeslotId);
    }
  }
  save(){
    if(this.data.hotelId ==0)
      return;
    this.api.AddRoom(this.data).subscribe((data)=>{
      this.notify.success("Room has been added succesfully");
    },(error)=>{
      this.notify.error(JSON.stringify(error));
    }
      
    );

  }
  

}
