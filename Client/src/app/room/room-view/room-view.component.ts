import { Room } from './../../models';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-room-view',
  templateUrl: './room-view.component.html',
  styleUrls: ['./room-view.component.css']
})
export class RoomViewComponent implements OnInit {
  roomId : number;
  room : Room;
  constructor(private route : ActivatedRoute,private api : ApiService,private notify : ToastrService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      this.roomId = parseInt(params.roomId);
      this.api.GetRoom(this.roomId).subscribe(roomData=>{
        this.room = roomData;
      },error=>{
        this.notify.error(error);
      })
    });
  }


}
