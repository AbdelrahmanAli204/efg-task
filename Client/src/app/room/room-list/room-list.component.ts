import { ToastrService } from 'ngx-toastr';
import { Room } from './../../models';
import { ApiService } from './../../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {
  hotelId : number;
  rooms : Room[];
  constructor(private api : ApiService,private route : ActivatedRoute,private notify : ToastrService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      this.hotelId = parseInt(params.hotelId);
      this.api.GetHotelRooms(this.hotelId).subscribe(rooms=>{
        this.rooms = rooms;
      },error=>{
        this.notify.error(error);
      });
    })
  }

}
